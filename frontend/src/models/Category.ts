export default class Category {
    constructor(
      public id: number,
      public name: string,
      public description: string,
      public unit: string,
      public unit_price: number) {}
  }
  