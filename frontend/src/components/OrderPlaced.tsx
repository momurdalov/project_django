import React from 'react';
import './OrderPlaced.css';

const OrderPlaced: React.FC = () => {

  return (
    <div className="outer">
        <img className="inner image-verify" src="../../assets/icon/verify.png"></img>
        <p> <strong>Ваш заказ успешно оформлен</strong></p>
        <p>Скоро на вашу заявку откликнутся, ближайшие к вам водители</p>
    </div>
  );
};
export default OrderPlaced;
