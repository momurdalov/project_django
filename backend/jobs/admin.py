from django.contrib import admin

from .models import Job, JobImage, Category, CategoryVehicleType,  ReviewImage, Order, OrderStatus, \
    OrderCancellation, JobStatus, Offer, Review, UserReason, DriverReason

admin.site.register(Job)
admin.site.register(JobImage)
admin.site.register(Category)
admin.site.register(CategoryVehicleType)
admin.site.register(ReviewImage)
admin.site.register(Offer)
admin.site.register(Review)
admin.site.register(Order)
admin.site.register(OrderCancellation)
